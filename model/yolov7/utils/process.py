import numpy as np
import torch
from typing import TypeVar


from model.yolov7.utils.general import check_img_size, scale_coords
from model.yolov7.utils.datasets import letterbox


def image_preprocessing(
    img: np.ndarray, model: TypeVar("models.yolo.Model"), imgsz: int
) -> torch.Tensor:
    """
    Preprocess images before YOLO model prediction

    Parameters
    ----------
    model: models.yolo.Model
        The object detection model to use
    img: np.ndarray
        The input image for preprocessing (H, W, C)
    imgsz: int
        The designated size for input image

    Returns
    -------
    img: torch.Tensor
        The preprocessed image as a torch tensor (1, C, H, W)
    """

    # Get Model Stride
    stride = int(model.stride.max())

    # Check imgsz
    imgsz = check_img_size(max(imgsz), s=stride)

    # Padded resize
    img = letterbox(img, imgsz, stride)[0]

    # Convert
    img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB
    img = np.ascontiguousarray(img)

    # Turn to torch tensor
    img = torch.from_numpy(img)

    # uint8 to fp16/32
    img = img.cuda().half() if torch.cuda.is_available() else img.float()

    # Normalize
    img /= 255.0

    # Check dimension
    if img.ndimension() == 3:
        img = img.unsqueeze(0)

    return img


def result_postprocess(
    _result: torch.Tensor, img_processed: torch.Tensor, img_org: np.ndarray
) -> dict:
    """
    Postprocess the object detected results and return them as a list of dictionaries.

    Parameters
    ----------
    _result: torch.Tensor
        A torch Tensor of unprocessed object detected result, where each element
        is a 6-dimensional torch tensor of shape (xmin, ymin, xmax, ymax, conf,
        classes)
    img_processed: torch.Tensor
        The image that after image preprocessing
    img_org: np.ndarray
        The original image directly from footage

    Returns
    -------
    result_post: dict
        A list of dictionaries, where each dictionary contains the bounding box,
        confidence, and class ID
    """
    # bounding boxes scaling
    _result[:, :4] = scale_coords(
        img_processed.shape[2:], _result[:, :4], img_org.shape
    ).round()

    result_post = {
        "bboxes": _result[:, :4].cpu().numpy().astype(int),
        "scores": _result[:, 4].cpu().numpy(),
        "class_ids": _result[:, 5].cpu().numpy().astype(int),
    }

    return result_post
