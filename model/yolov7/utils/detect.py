import torch
from typing import TypeVar

from model.yolov7.utils.general import non_max_suppression


def detect(
    img: torch.Tensor,
    model: TypeVar("models.yolo.Model"),
    conf_thresh: float,
    iou_thresh: float,
) -> torch.Tensor:
    """
    Detect objects in an image using a given model, followed by non-max suppression

    Parameters
    ----------
    model: models.yolo.Model
        The object detection model to use
    img: torch.Tensor
        The input image to detect object in
    conf_thresh: float
        The confidence threshold for detections for Non-Maximum Suppression
    iou_thresh: float
        The Intersection over Union threshold for Non-Maximum Suppression

    Returns:
    pred: torch.Tensor
        The output of the object detection model after applying Non-Maximum
        Suppression
    """
    with torch.no_grad():
        pred = model(img)[0]

    # Apply NMS
    pred = non_max_suppression(pred, conf_thresh, iou_thresh)

    return pred[0]
