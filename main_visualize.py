import os
import random
import cv2
from dotenv import load_dotenv

from model.yolov7 import image_preprocessing, result_postprocess, detect, draw_bboxes

from main import YOLOv7_inference

load_dotenv()


class YOLOv7_inference_visualize(YOLOv7_inference):
    """
    Class inherit with the YOLOv7_inference. This class is used for visualize
    the detection result only

    Parameters
    ----------
    model_path: str
        path of the yolov7 model file (in .pt format)

    Attributes
    ----------
    conf_thresh: float
        Confidence threshold for the object detection
    iou_thresh: float
        IoU threshold for non-maximum suppression.
    imgsz: tuple
        Size of input images for inferencing
    colors: dict
        Colors for each of object labels which is assigned by random number
    """

    def __init__(self, model_path: str) -> None:
        super().__init__(model_path)
        self.colors = self.__assign_object_colors(self.object_list)

    def __call__(self, path: str):
        if path.endswith((".jpg", ".png")):
            return self.run_image_source(path)
        elif path.endswith((".mp4", ".avi", ".MP4")):
            return self.run_video_source(path)

    def __assign_object_colors(self, object_list: list) -> dict:
        """
        Assign random colors to a list of objects

        Parameters
        ----------
        object_list: list
            A list contains all of the objects for detection

        Returns
        -------
        color: dict
            A dictionary of colors indexed by the object indices
        """
        # Define Dict
        colors = {}

        for i in range(len(object_list)):
            colors[i] = tuple([random.randint(0, 255) for _ in range(3)])

        return colors

    def run_image_source(self, file_path: str) -> None:
        """
        Run inference on an image file

        Parameters
        ----------
        file_path: str
            The path to the image file.

        Returns
        -------
        Saved image with bboxes and object labels
        """

        # Extract filename
        filename = os.path.basename(file_path).split(".")[0]

        # load image
        img = cv2.imread(file_path)

        # image preprocessing
        img_processed = image_preprocessing(img, self.model, self.imgsz)

        # Detect
        result = detect(img_processed, self.model, self.conf_thresh, self.iou_thresh)

        # Early return if no object can be detected
        if not result.nelement():
            return None

        # Prediction result postprocessing
        result_post = result_postprocess(result, img_processed, img)

        # Early return
        if not result_post:
            return None

        # Draw bboxes
        img_bboxes = draw_bboxes(img, result_post, self.colors, self.object_list)

        # Save drawed result
        cv2.imwrite(f"{filename}_result.jpg", img_bboxes)

    def run_video_source(self, file_path: str) -> None:
        """
        Run inference on an video file

        Parameters
        ----------
        file_path: str
            The path to the video file.

        Returns
        -------
        Saved video with bboxes and object labels
        """
        # Get videocapture ready
        cap = cv2.VideoCapture(file_path)

        # Get the image width and height
        img_height, img_width = int(cap.get(3)), int(cap.get(4))

        # Extract file name and format from file_path
        file_name, file_format = os.path.basename(file_path).split(".")

        # Define VideoWriter object
        out = cv2.VideoWriter(
            f"./{file_name}_processed.{file_format}",
            cv2.VideoWriter_fourcc(*"mp4v"),
            20,
            (img_height, img_width),
        )

        while cap.isOpened():
            # Read camera footage
            ret, frame = cap.read()

            # Early return
            if not ret:
                break

            # frame preprocessing
            frame_processed = image_preprocessing(frame, self.model, self.imgsz)

            # Detect
            result = detect(
                frame_processed, self.model, self.conf_thresh, self.iou_thresh
            )

            # Early return if no object can be detected
            if not result.nelement():
                out.write(frame)
                continue

            # Prediction result postprocessing
            result_post = result_postprocess(result, frame_processed, frame)

            # Early return
            if not result_post:
                continue

            # Draw bboxes
            img_bboxes = draw_bboxes(frame, result_post, self.colors, self.object_list)

            # write video
            out.write(img_bboxes)

        return None


if __name__ == "__main__":
    model_path = os.environ.get("YOLOv7_model_path")

    yolov7_inference_visualize = YOLOv7_inference_visualize(model_path)
    yolov7_inference_visualize("./images/dog.jpg")
