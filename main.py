import os
import numpy as np
import cv2
import torch
from dotenv import load_dotenv
from typing import TypeVar

from model.yolov7 import attempt_load, image_preprocessing, result_postprocess, detect

load_dotenv()


class YOLOv7_inference:
    """
    Class for inference using YOLOv7 object detection algorithm on loaded images

    Parameters
    ----------
    model_path: str
        path of the yolov7 model file (in .pt format)

    Attributes
    ----------
    conf_thresh: float
        Confidence threshold for the object detection
    iou_thresh: float
        IoU threshold for non-maximum suppression.
    imgsz: tuple
        Size of input images for inferencing
    device: torch.device
        Device to run the models on (cuda or cpu)
    model: models.yolo.Model
        Loaded YOLOv7 model
    object_list: dict
        A dictionary contains all class labels and their corresponding class id
    """

    def __init__(self, model_path: str) -> None:
        self.conf_thresh = float(os.environ.get("CONF_THRESH"))
        self.iou_thresh = float(os.environ.get("IOU_THRESH"))
        self.imgsz = eval(os.environ.get("IMGSZ"))
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.model = self.__load_yolov7_model(model_path)
        self.object_list = self.model.names

    def __call__(self, img: np.ndarray) -> dict:
        """
        Perform object detection using YOLOv7 algorithm.

        Parameters
        ----------
        img: np.ndarray
            The loaded image for object detection in shape H, W, C

        Returns
        -------
        result_post: list[dict]
            A list of dictionary contains bbox coordinates, confidence, and
            detected class id
        """
        # image preprocessing
        img_processed = image_preprocessing(img, self.model, self.imgsz)

        # Detect
        result = detect(img_processed, self.model, self.conf_thresh, self.iou_thresh)

        # Early return if no objects can be detected
        if not result.nelement():
            return None

        # Prediction result postprocessing
        result_post = result_postprocess(result, img_processed, img)

        return result_post

    def __load_yolov7_model(self, model_path: str) -> TypeVar("model.yolo.Model"):
        """
        Load the YOLOv7 model from a given path

        Parameters
        ----------
        model_path: str
            The path to the YOLOv7 model

        Returns
        -------
        model: models.yolo.Model
            The loaded YOLOv7 model
        """

        if not os.path.exists(model_path):
            raise FileExistsError("Model weight is not exist in saved_model folder")

        # Attemp loading FP32 model
        model = attempt_load(model_path, self.device)

        # Half precision
        if torch.cuda.is_available():
            model.half()

        return model


if __name__ == "__main__":
    model_path = os.environ.get("YOLOv7_model_path")

    yolov7_inference = YOLOv7_inference(model_path)

    img = cv2.imread("./images/dog.jpg")
    detection_result = yolov7_inference(img)
